﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Impostor.Api.Events;
using Impostor.Api.Net.Inner.Objects;
using Impostor.Api.Events.Player;
using Impostor.Api.Innersloth;
using Microsoft.Extensions.Logging;
using my.AtheroX.AmongUsPlus;
using Impostor.Api.Net;

namespace my.AtheroX.AllOfUs.Handlers {
	public class GameEventListener : IEventListener {
		private readonly ILogger<AmongUsPlusPlugin> _logger;
		static System.Random rnd = new System.Random();

		struct JesterGame {
			public int JesterClientId;
			public bool JesterOn;
			public bool Jesterwin;
			public bool GameEnded;
			public bool CountingDown;
			public bool JesterInGame;
			public string Jestername;
		}
		Dictionary<string, JesterGame> JesterGames = new Dictionary<string, JesterGame>();

		public GameEventListener(ILogger<AmongUsPlusPlugin> logger) {
			_logger = logger;
		}

		public async ValueTask<int> SendMessage(IInnerPlayerControl player, string message) {
			string name = player.PlayerInfo.PlayerName;
			byte color = player.PlayerInfo.ColorId;
			await player.SetNameAsync("[FF0000FF]Intento de bot");
			await player.SetColorAsync((byte)0);
			await player.SendChatAsync(message);
			await player.SetNameAsync(name);
			await player.SetColorAsync(color);
			return 0;
		}
		private async Task SendPrivateMessage(IInnerPlayerControl player, string message) {
			string playername = player.PlayerInfo.PlayerName;
			await player.SetNameAsync($"[FF0000FF]Intento de bot").ConfigureAwait(false);
			await player.SendChatToPlayerAsync($"{message}", player).ConfigureAwait(false);
			await player.SetNameAsync(playername);
		}

		[EventListener]
		public async void OnLobbyCreate(IPlayerSpawnedEvent e) {
			if (e.ClientPlayer.IsHost) {
				await Task.Delay(TimeSpan.FromSeconds(1.5)).ConfigureAwait(false);
				await SendPrivateMessage(e.PlayerControl, "/help");
			}

			if (JesterGames[e.Game.Code].GameEnded && JesterGames[e.Game.Code].JesterInGame) {
				Task.Run(async () => await JesterAnnouncement(e).ConfigureAwait(false));
			}
		}

		private async Task JesterAnnouncement(IPlayerSpawnedEvent e) {
			await Task.Delay(TimeSpan.FromSeconds(1.5)).ConfigureAwait(false);
			await SendPrivateMessage(e.PlayerControl, $"El jester, {JesterGames[e.Game.Code].Jestername}, " +
				$"{(JesterGames[e.Game.Code].Jesterwin ? "ganó" : "perdió") } la anterior partida").ConfigureAwait(false);
		}


		[EventListener]
		public async void OnPlayerChat(IPlayerChatEvent e) {

			_logger.LogInformation($"{e.PlayerControl.PlayerInfo.PlayerName} ha dicho {e.Message}");


			string[] args = e.Message.Trim().ToLower().Split(' ');
			string name = e.PlayerControl.PlayerInfo.PlayerName;
			byte color = e.PlayerControl.PlayerInfo.ColorId;
			Dictionary<string, int> Colors = new Dictionary<string, int>();
			Colors.Add("red", 0);
			Colors.Add("blue", 1);
			Colors.Add("green", 2);
			Colors.Add("pink", 3);
			Colors.Add("orange", 4);
			Colors.Add("yellow", 5);
			Colors.Add("black", 6);
			Colors.Add("white", 7);
			Colors.Add("purple", 8);
			Colors.Add("brown", 9);
			Colors.Add("cyan", 10);
			Colors.Add("lime", 11);
			if (e.Game.GameState == GameStates.NotStarted) {
				switch (args[0]) {
					case "/help":
					case "/h":
						if (e.ClientPlayer.IsHost) {
							await SendPrivateMessage(e.PlayerControl, "Comandos: /map (m), /name (n), /color (c), /players (p), /impostors (i), /help (h), /gamemodes (g)");
							await SendPrivateMessage(e.PlayerControl, "Escribe el comando para ver como se usa (/<comando>)");
						} else {
							await SendPrivateMessage(e.PlayerControl, "Comandos: /name (n), /color (c), /help (h)");
							await SendPrivateMessage(e.PlayerControl, "Escribe el comando para ver como se usa (/<comando>)");
						}
						break;
					case "/gamemode":
					case "/gamemodes":
					case "/g":
						if (e.ClientPlayer.IsHost) {
							if (args.Length == 1) {

								await SendPrivateMessage(e.PlayerControl, "Gamemodes: Jester (j), Black (b)");
								await SendPrivateMessage(e.PlayerControl, "Escribe el /gamemode (g) (<Gamemode>)");
							} else {
								switch (args[1]) {
									case "jester":
									case "j":
										await SendPrivateMessage(e.PlayerControl, "/jester (j)");
										await SendPrivateMessage(e.PlayerControl, "Un nuevo rol llamado Jester, este debe conseguir que los demás le voten fuera durante una votación");
										await SendPrivateMessage(e.PlayerControl, "Durante el cooldown de empezar la partida, abre el chat para saber si eres el Jester");
										await SendPrivateMessage(e.PlayerControl, "En caso de ser impostor, el jester será dado a otro jugador");
										break;
									case "black":
									case "b":
										await SendPrivateMessage(e.PlayerControl, "/black (b)");
										await SendPrivateMessage(e.PlayerControl, "Un modo de juego donde todos tienen el mismo color y el mismo nombre");
										break;
								}
							}
						}
						break;
					case "/color":
					case "/c":
						if (args.Length > 1) {
							if (Colors.ContainsKey(args[1])) {
								await SendMessage(e.PlayerControl, $"Color cambiado a {args[1]}!");
								await e.PlayerControl.SetColorAsync((byte)Colors[args[1]]);
								break;
							} else {
								await SendMessage(e.PlayerControl, "[FF0000FF]Color no válido");
								await SendMessage(e.PlayerControl, "Colores: Red, Blue, Green, Pink, Orange, Yellow, Black, White, Purple, Brown, Cyan, Lime");
								break;
							}
						} else if (args.Length == 1) {
							await SendMessage(e.PlayerControl, "/color (c) {color}");
							await SendMessage(e.PlayerControl, "Colores: Red, Blue, Green, Pink, Orange, Yellow, Black, White, Purple, Brown, Cyan, Lime");
						}
						break;
					case "/name":
					case "/n":
						if (args.Length > 1) {
							if (args[1].Length < 11 && args[1].Length > 3) {
								bool nameUsed = false;
								await SendMessage(e.PlayerControl, $"Nombre cambiado a {args[1]}");
								await e.PlayerControl.SetNameAsync($"{args[1]}");
							} else {
								await SendMessage(e.PlayerControl, "El nombre es muy corto o muy largo. Sólo de 3 a 11 carácteres");
							}
						} else if (args.Length == 1) {
							await SendMessage(e.PlayerControl, "/name (n) {name}");
						}
						break;

					case "/impostors":
					case "/impostor":
					case "/i":
						if (e.ClientPlayer.IsHost) {
							if (args.Length > 1) {
								bool tryLimit = int.TryParse(args[1], out int limit);
								if (tryLimit) {
									if (limit < 127 && limit > 0) {
										if ((e.Game.Options.MaxPlayers / limit) > 2f) {
											e.Game.Options.NumImpostors = (byte)limit;
											await SendMessage(e.PlayerControl, $"La cantidad de impostores ha sido puesta en {args[1]}");
											await e.Game.SyncSettingsAsync();
										} else {
											await SendMessage(e.PlayerControl, "Te has pasado");
										}
									} else {
										await SendMessage(e.PlayerControl, "[FF0000FF]Error: La cantidad de impostores debe ser mayor de 0 y menor de 64");
									}
								} else {
									await SendMessage(e.PlayerControl, "[FF0000FF]Error: Introduce un número, perro");
								}
							}
							if (args.Length == 1) {
								await SendMessage(e.PlayerControl, "/impostors (i) {amount}\nPone la cantidad de impostores. De 1 a 63");
							}
						} else {
							await SendMessage(e.PlayerControl, "[FF0000FF] No tienes permisos tonto");
						}
						break;
					case "/players":
					case "/maxplayers":
					case "/p":
						if (e.ClientPlayer.IsHost) {
							if (args.Length > 1) {
								bool tryLimit = int.TryParse(args[1], out int limit);
								if (tryLimit) {
									if (limit < 127 && limit > 4) {
										e.Game.Options.MaxPlayers = (byte)limit;
										await SendMessage(e.PlayerControl, $"Límite de jugadores puesto en {args[1]}");
										await e.Game.SyncSettingsAsync();
									} else {
										await SendMessage(e.PlayerControl, "[FF0000FF]Error: El límite debe ser entre 3 y 128");
										await e.Game.SyncSettingsAsync();
									}
								} else {
									await SendMessage(e.PlayerControl, "[FF0000FF]Error: Introduce un número, perro");
								}
							}
							if (args.Length == 1) {
								await SendMessage(e.PlayerControl, "/players (p) {amount}\nEstablece el límite de jugadores. Debe ser entre 3 y 128");
							}
						} else {
							await SendMessage(e.PlayerControl, "[FF0000FF] No tienes permisos tonto");
						}
						break;

					case "/map":
					case "/m":
						if (e.ClientPlayer.IsHost) {
							if (args.Length > 1) {
								string mapname = args[1];
								switch (mapname) {
									case "skeld":
									case "1":
										e.Game.Options.Map = (MapTypes)(byte)MapTypes.Skeld;
										await e.Game.SyncSettingsAsync();
										await SendMessage(e.PlayerControl, "Mapa: The Skeld");
										break;
									case "mira":
									case "mirahq":
									case "2":
										e.Game.Options.Map = (MapTypes)(byte)MapTypes.MiraHQ;
										await e.Game.SyncSettingsAsync();
										await SendMessage(e.PlayerControl, "Mapa: MiraHQ");
										break;
									case "polus":
									case "3":
										e.Game.Options.Map = (MapTypes)(byte)MapTypes.Polus;
										await e.Game.SyncSettingsAsync();
										await SendMessage(e.PlayerControl, "Mapa: Polus");
										break;
									default:
										await SendMessage(e.PlayerControl, "[FF0000FF]Mapa erroneo. Mapas: Skeld(1), Mira/MiraHQ(2), Polus(3)");
										break;
								}
							}
							if (args.Length == 1) {
								await SendMessage(e.PlayerControl, "/map (m) {map}\nSelecciona el mapa. Mapas: Skeld(1), Mira/MiraHQ(2), Polus(3)");
							}
						} else await SendMessage(e.PlayerControl, "[FF0000FF] No tienes permisos tonto");

						break;

					case "/jester":
					case "/j":
						if (e.ClientPlayer.IsHost) {
							JesterGame jester = JesterGames[e.Game.Code];
							jester.JesterOn = !jester.JesterOn;
							JesterGames[e.Game.Code] = jester;
							await SendMessage(e.PlayerControl, $"JesterMode: {(jester.JesterOn?"Activado":"Desactivado")}");
						} else await SendMessage(e.PlayerControl, "[FF0000FF] No tienes permisos tonto");

						break;

					case "/black":
					case "/b":
						if (e.ClientPlayer.IsHost) {
							foreach (var p in e.Game.Players) {
								await p.Character.SetColorAsync(colorType: Impostor.Api.Innersloth.Customization.ColorType.Black).ConfigureAwait(false);
								await p.Character.SetNameAsync(name: "????").ConfigureAwait(false);
							}
						}
						break;
				}
			}
		}

		[EventListener]
		public void OnGameCreated(IGameCreatedEvent e) {
			JesterGame jester = new JesterGame();
			jester.JesterOn = false;
			jester.Jesterwin = false;
			jester.GameEnded = false;
			jester.CountingDown = false;
			jester.JesterInGame = false;
			JesterGames.Add(e.Game.Code, jester);
		}

		[EventListener]
		public void OnSetStartCounter(IPlayerSetStartCounterEvent e) {
			if (e.SecondsLeft == 5) {
				JesterGame jester = JesterGames[e.Game.Code];
				jester.CountingDown = true;
				JesterGames[e.Game.Code] = jester;

				_logger.LogInformation($"Empieza la cuenta atrás");
				if (JesterGames[e.Game.Code].JesterOn) {
					Task.Run(async () => await AssignJester(e).ConfigureAwait(false));
					foreach (var player in e.Game.Players) {
						Task.Run(async () => await MakePlayerLookAtChat(player).ConfigureAwait(false));
					}
				}
			}
		}

		private async Task AssignJester(IPlayerSetStartCounterEvent e) {
			List<IClientPlayer> players = new List<IClientPlayer>();
			foreach (var player in e.Game.Players) {
				players.Add(player);
			}
			int r = rnd.Next(players.Count);

			JesterGame jester = JesterGames[e.Game.Code];
			jester.JesterClientId = players[r].Client.Id;
			jester.Jestername = players[r].Character.PlayerInfo.PlayerName;
			JesterGames[e.Game.Code] = jester;

			await SendPrivateMessage(players[r].Character, $"Eres el jester! (En caso de ser impostor se hará reroll al rol)").ConfigureAwait(false);
			_logger.LogInformation($"{JesterGames[e.Game.Code].Jestername} es el Jester. (En caso de ser impostor se hará reroll al rol)");
		}

		private async Task AssignJester(IGameStartedEvent e) {
			List<IClientPlayer> players = new List<IClientPlayer>();
			foreach (var player in e.Game.Players) {
				players.Add(player);
			}
			int r = rnd.Next(players.Count);

			JesterGame jester = JesterGames[e.Game.Code];
			jester.JesterClientId = players[r].Client.Id;
			jester.Jestername = players[r].Character.PlayerInfo.PlayerName;
			JesterGames[e.Game.Code] = jester;

			await SendPrivateMessage(players[r].Character, $"Eres el jester! (En caso de ser impostor se hará reroll al rol)").ConfigureAwait(false);
			_logger.LogInformation($"{JesterGames[e.Game.Code].Jestername} es el Jester. (En caso de ser impostor se hará reroll al rol)");
		}

		private async Task MakePlayerLookAtChat(IClientPlayer player) {
			await Task.Delay(TimeSpan.FromSeconds(0.5)).ConfigureAwait(false);
			string playername = player.Character.PlayerInfo.PlayerName;
			await player.Character.SetNameAsync($"ABRE EL CHAT").ConfigureAwait(false);
			await Task.Delay(TimeSpan.FromSeconds(3)).ConfigureAwait(false);
			await player.Character.SetNameAsync(playername).ConfigureAwait(false);
		}

		[EventListener]
		public void OnGamePlayerJoined(IGamePlayerJoinedEvent e) {
			if (JesterGames[e.Game.Code].CountingDown) {
				_logger.LogInformation($"Entrega de rol interrumpida porque alguien ha entrado. {JesterGames[e.Game.Code].Jestername} ya no es el Jester");
				Task.Run(async () => await SorryNotJester(e.Game.GetClientPlayer(JesterGames[e.Game.Code].JesterClientId)).ConfigureAwait(false));

				JesterGame jester = JesterGames[e.Game.Code];
				jester.CountingDown = false;
				JesterGames[e.Game.Code] = jester;
			}
		}

		[EventListener]
		public void OnGamePlayerLeft(IGamePlayerLeftEvent e) {
			if (JesterGames[e.Game.Code].CountingDown) {
				_logger.LogInformation($"Entrega de rol interrumpida porque un jugador ha salido. {JesterGames[e.Game.Code].Jestername} ya no es el Jester");
				Task.Run(async () => await SorryNotJester(e.Game.GetClientPlayer(JesterGames[e.Game.Code].JesterClientId)).ConfigureAwait(false));

				JesterGame jgame = JesterGames[e.Game.Code];
				jgame.CountingDown = false;
				JesterGames[e.Game.Code] = jgame;
			}
		}

		private async Task SorryNotJester(IClientPlayer player) {
			await SendMessage(player.Character, $"Interrumpido, no eres el Jester").ConfigureAwait(false);
		}

		[EventListener]
		public void OnGameStarted(IGameStartedEvent e) {
			JesterGame jester = JesterGames[e.Game.Code];
			jester.GameEnded = false;
			jester.CountingDown = false;
			jester.JesterInGame = false;
			jester.Jesterwin = false;
			JesterGames[e.Game.Code] = jester;

			_logger.LogInformation($"Empieza el juego");
			if (JesterGames[e.Game.Code].JesterOn) {
				Task.Run(async () => await InformJester(e).ConfigureAwait(false));
			}
			// This prints out for all players if they are impostor or crewmate.
			foreach (var player in e.Game.Players) {
				if (JesterGames[e.Game.Code].JesterOn && (player.Character.PlayerInfo.HatId == 27 || player.Character.PlayerInfo.HatId == 84)) {
					Task.Run(async () => await OffWithYourHat(player).ConfigureAwait(false));
				}
				var info = player.Character.PlayerInfo;
				var isImpostor = info.IsImpostor;
				if (isImpostor) {
					_logger.LogInformation($"- {info.PlayerName} es el impostor");
				} else {
					_logger.LogInformation($"- {info.PlayerName} es un tripulante");
				}
			}
		}

		private async Task InformJester(IGameStartedEvent e) {
			if (e.Game.GetClientPlayer(JesterGames[e.Game.Code].JesterClientId).Character.PlayerInfo.IsImpostor) {
				_logger.LogInformation($"- {e.Game.GetClientPlayer(JesterGames[e.Game.Code].JesterClientId).Character.PlayerInfo.PlayerName} iba a ser Jester, pero es impostor");
				await SendPrivateMessage(e.Game.GetClientPlayer(JesterGames[e.Game.Code].JesterClientId).Character, $"Como eres impostor no serás Jester").ConfigureAwait(false);
				await Task.Run(async () => await AssignJester(e).ConfigureAwait(false));
				await Task.Run(async () => await InformJester(e).ConfigureAwait(false));
			} else {
				_logger.LogInformation($"- {e.Game.GetClientPlayer(JesterGames[e.Game.Code].JesterClientId).Character.PlayerInfo.PlayerName} es el Jester");
				await SendPrivateMessage(e.Game.GetClientPlayer(JesterGames[e.Game.Code].JesterClientId).Character, $"Eres el Jester").ConfigureAwait(false);

				JesterGame jester = JesterGames[e.Game.Code];
				jester.JesterInGame = true;
				JesterGames[e.Game.Code] = jester;
			}
		}

		private async Task OffWithYourHat(IClientPlayer player) {
			await player.Character.SetHatAsync(Impostor.Api.Innersloth.Customization.HatType.NoHat).ConfigureAwait(false);
		}

		[EventListener]
		public void OnPlayerExiled(IPlayerExileEvent e) {
			if (JesterGames[e.Game.Code].JesterInGame && e.PlayerControl == e.Game.GetClientPlayer(JesterGames[e.Game.Code].JesterClientId).Character) {
				JesterGame jester = JesterGames[e.Game.Code];
				jester.Jesterwin = true;
				JesterGames[e.Game.Code] = jester;

				_logger.LogInformation($"Ha ganado el Jester");
				Task.Run(async () => await TurnTheTables(e).ConfigureAwait(false));
			}
		}

		private async Task TurnTheTables(IPlayerExileEvent e) {
			await e.Game.GetClientPlayer(JesterGames[e.Game.Code].JesterClientId).Character.SetHatAsync(Impostor.Api.Innersloth.Customization.HatType.ElfHat).ConfigureAwait(false);
			foreach (var player in e.Game.Players) {
				if (player.Client.Id != JesterGames[e.Game.Code].JesterClientId) {
					await player.Character.SetHatAsync(Impostor.Api.Innersloth.Customization.HatType.DumSticker).ConfigureAwait(false);
				}
			}
			await Task.Delay(TimeSpan.FromSeconds(1.5)).ConfigureAwait(false);

			foreach (var player in e.Game.Players) {
				if (!player.Character.PlayerInfo.IsDead && player.Character.PlayerInfo.IsImpostor) {
					await player.Character.SetMurderedByAsync(player);
				}
			}
		}

		[EventListener]
		public void OnGameEnded(IGameEndedEvent e) {
			_logger.LogInformation($"Fin del juego");

			JesterGame jester = JesterGames[e.Game.Code];
			jester.GameEnded = true;
			JesterGames[e.Game.Code] = jester;
		}

		[EventListener]
		public void OnGameDestroyed(IGameDestroyedEvent e) {
			JesterGames.Remove(e.Game.Code);
		}

	}
}